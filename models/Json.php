<?php

namespace models;

/**
 * Class Json
 * @package models
 */
class Json
{
    /**
     * Data of json
     * @var string
     */
    public $data;

    /**
     * Errors
     * @var array
     */
    public $error = [];

    /**
     * Filepath to local file
     * @var string
     */
    private $_file = __DIR__ . '/../runtime/test.json';

    /**
     * URL to remote file
     * @var string
     */
    private $_urlOfFile = 'http://elektronnyigorod.local/test.json';

    /**
     * Json constructor.
     */
    public function __construct()
    {
        $this->getJsonFromUrl();

        $data = file_get_contents($this->_file);
        $this->data = str_replace(["\r\n", ' '], '', $data);
    }

    /**
     * @return $this
     */
    private function getJsonFromUrl()
    {
        if (($json = @file_get_contents($this->_urlOfFile)) === false) {
            $this->error = ['message' => 'Cant get file from remote URL. Refresh page to retry'];
        }

        $json = @file_get_contents($this->_urlOfFile);

        if (empty($this->error)) {
            file_put_contents($this->_file, $json);
        }

        return $this;
    }
}