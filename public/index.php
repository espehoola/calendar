<?php

require (__DIR__ . '/../models/Json.php');

use models\Json;

$json = new Json();
$jsonArray = json_decode($json->data, true);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/bootstrap-datepicker3.standalone.css">
    <link rel="stylesheet" href="css/eventCalendar.css">
    <link rel="stylesheet" href="css/eventCalendar_theme_responsive.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="js/moment.js"></script>
    <script src="js/jquery.eventCalendar.js"></script>
    <script>
        $(function(){
            /*var data = [
                { "date": "2015-09-21 10:15:20", "title": "Событие 1", "description": "Lorem Ipsum dolor set", "url": "http://www.event3.com/" },
                { "date": "2015-09-21 10:15:20", "title": "Событие 2", "description": "Lorem Ipsum dolor set", "url": "" },
                { "date": "2015-09-01 10:15:20", "title": "Событие 3", "description": "Lorem Ipsum dolor set", "url": "http://www.event3.com/" },
                { "date": "2015-10-21 10:15:20", "title": "Событие 4", "description": "Lorem Ipsum dolor set", "url": "http://www.event3.com/" },
            ];*/
            var availableHours = <?= json_encode($jsonArray['availableHours']) ?>;
            var holidays = <?= json_encode($jsonArray['holidays']) ?>;
            $('#eventCalendar').eventCalendar({
                // jsonData: data,
                eventsjson: 'data.json',
                jsonDateFormat: 'human',
                startWeekOnMonday: false,
                openEventInNewWindow: true,
                dateFormat: 'dddd DD-MM-YYYY',
                showDescription: false,
                locales: {
                    locale: "ru",
                    txt_noEvents: "Нет запланированных событий",
                    txt_SpecificEvents_prev: "",
                    txt_SpecificEvents_after: "события:",
                    txt_NextEvents: "Следующие события:",
                    txt_GoToEventUrl: "Смотреть",
                    moment: {
                        "months" : [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
                        "monthsShort" : [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн",
                            "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
                        "weekdays" : [ "Воскресенье", "Понедельник","Вторник","Среда","Четверг",
                            "Пятница","Суббота" ],
                        "weekdaysShort" : [ "Вс","Пн","Вт","Ср","Чт",
                            "Пт","Сб" ],
                        "weekdaysMin" : [ "Вс","Пн","Вт","Ср","Чт",
                            "Пт","Сб" ]
                    }
                }
            });
        });
    </script>
    <title>Test</title>
</head>
<body>
<div class="container">

    <?php if (!empty($json->error)) { ?>
        <?= $json->error['message'] ?>
    <?php } ?>

    <div id="eventCalendar" style="width: 300px; margin: 50px auto;"></div>

</div>

</body>
</html>